(function (angular) {
    'use strict';
    angular.module('core').service('feedbackService', feedbackService);

    //feedbackService.$inject = [];
    function feedbackService() {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);
            object.saveFields = ['mood','fatigue','hardness','pain','where','comments','fullFeedback','coachComment','coachFeedback', 'flgUserView','answer'];
            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            object.attributes.createdAt = moment(object.parseObject.createdAt).format('DD-MM-YYYY');
            object.attributes.printDateRange =
                moment(object.startDate).format('DD-MM-YYYY') + ' - ' +
                moment(object.endDate).format('DD-MM-YYYY');

            if ( parseObject.get('user') ){
                object.userId = parseObject.get('user').id;
                object.attributes.user = parseObject.get('user');
                object.username = parseObject.get('user').get('username')

            }
            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            object.getCreatedAt = function () {
                return this.parseObject.createdAt;
            };


            object.full = function (data, isCoach) {

                data['fullFeedback']    = true;
                data['flgUserView']     = false;
                data['answer']     = data.answer;
                if ( isCoach ){
                    data['coachFeedback'] = true;
                }
                object.update(data);

            };

            object.update = function (data, callback) {
                var i;
                for (i in data) {
                    if ( object.saveFields.indexOf(i)==-1){
                        continue;
                    }
                    this.parseObject.set(i, data[i]);
                }
                this.parseObject.save(null, {
                    success: function (answer) {
                        if (callback)
                            callback({success: true});
                    },
                    error: function (answer, error) {
                        if (callback)
                            callback({success: false, error: error.message});
                    }
                })
            };

            object.delete = function (callback) {
                this.parseObject.destroy({
                    success: function (myObject) {
                        callback({success:true})
                    },
                    error: function (myObject, error) {
                        callback({success:false,error:error})
                    }
                });
            };
            object.checkForm = function (isCoach) {
                if ( this.mood==undefined ){
                    return true;
                }
                if ( this.fatigue==undefined ){
                    return true;
                }
                if ( this.hardness==undefined ){
                    return true;
                }
                if ( this.pain==undefined ){
                    return true;
                }
                if ( this.pain=='Yes' && this.where==undefined  ){
                    return true;
                }
                /*if ( this.fullFeedback==true && isCoach==false){
                    return true;
                }
                if ( this.coachComment==undefined && isCoach==true){
                    return true;
                }
                if ( this.coachFeedback==true && isCoach==true){
                    return true;
                }*/
                return false;
            };



            return object;

        }
    }
})(angular);

