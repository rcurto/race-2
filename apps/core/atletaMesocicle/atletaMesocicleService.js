(function (angular) {
    'use strict';
    angular.module('core').service('atletaMesocicleService', atletaMesocicleService);

    //atletaMesocicleService.$inject = ['$filter'];
    function atletaMesocicleService() {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);

            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            object.attributes.createdAt = moment(object.parseObject.createdAt).format('DD-MM-YYYY');
            object.printData = moment(parseObject.get('data')).format('DD-MM-YYYY');


            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            object.getCreatedAt = function () {
                return this.parseObject.createdAt;
            };
            object.getDay = function (day) {
                return moment(object.get('datini') ).add(day,'day').format('dd')
            };
            object.dayField = function (day) {
                var fields = ['dilluns',
                    'dimarts',
                    'dimecres',
                    'dijous',
                    'divendres',
                    'dissabte',
                    'diumenge'];
                return  fields[day];
            };
            object.getWeek = function (week, day) {
                return moment(object.get('datini') ).add(week,'week').add(day,'day').format('DD/MM');
            };


            object.update = function (data, callback) {
                var i;
                for (i in data) {
                    if (i=='parseObject'){
                        continue;
                    }
                    this.parseObject.set(i, data[i]);
                }
                this.parseObject.save(data, {
                    success: function (answer) {
                        callback({success: true});
                    },
                    error: function (answer, error) {
                        callback({success: false, error: error.message});
                    }
                })
            };

            object.delete = function (callback) {
                this.parseObject.destroy({
                    success: function (myObject) {
                        callback({success:true})
                    },
                    error: function (myObject, error) {
                        callback({success:false,error:error})
                    }
                });
            };



            return object;

        }
    }
})(angular);

