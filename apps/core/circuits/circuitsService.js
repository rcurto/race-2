(function (angular) {
    'use strict';
    angular.module('core').service('circuitsService', circuitsService);

   // circuitsService.$inject = ['circuitsFactory'];
    function circuitsService() {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);

            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            object.attributes.createdAt = moment(object.parseObject.createdAt).format('DD-MM-YYYY');

            object.countryId = parseObject.get('country')!=undefined ? parseObject.get('country').id : null
            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            object.getCreatedAt = function () {
                return this.parseObject.createdAt;
            };
            object.getCurseData = function () {
                return moment(this.get('data')).format('DD-MM-YYYY')
            };

            object.update = function (data, callback) {
                var i;
                for (i in data) {
                    if (i=='parseObject'){
                        continue;
                    }
                    this.parseObject.set(i, data[i]);
                }
                this.parseObject.save(data, {
                    success: function (answer) {
                        callback({success: true});
                    },
                    error: function (answer, error) {
                        callback({success: false, error: error.message});
                    }
                })
            };

            object.delete = function (callback) {
                this.parseObject.destroy({
                    success: function (myObject) {
                        callback({success:true})
                    },
                    error: function (myObject, error) {
                        callback({success:false,error:error})
                    }
                });
            };



            return object;

        }
    }
})(angular);

