(function (angular) {
    'use strict';

    function headerMenuDirective() {
        return {
            replace:true,
            restrict: 'E',
            controller:headerMenuController,
            templateUrl: 'apps/mainApp/header-menu/header-menu.html',
            scope:{}
        };

        headerMenuController.$inject = ['$scope', 'userFactory', '$rootScope', '$state', '$timeout'];
        function headerMenuController($scope, userFactory, $rootScope, $state, $timeout){

            $scope.env={
                showLogin: false,
                user: undefined,
                pendingNotificationList : null,
                events: []
            };

            auth();
            $rootScope.$on('login', function() {
                auth();
            });
            $rootScope.$on('logout', function() {
                auth();
            });
            function auth(){
                userFactory.auth(function(user){
                    $scope.env.user = user;
                    user.getPendingNotifications().then(function(events){
                        $scope.env.events = events;
                    });

                    $scope.$apply()
                })
            }

            $scope.logout = function(){
                userFactory.logout();
                if (angular.element(document.getElementById('quicksidebarController')).scope().unsubscribe){
                    angular.element(document.getElementById('quicksidebarController')).scope().unsubscribe("ramonchnnel", $timeout);
                }
                window.location.href='index.html'
            };


            $scope.selectEvent = function(event){
                event.read();
                $state.go( event.getUrl() )
            }
        }
    }

    angular.module('MetronicApp').directive('headerMenu', headerMenuDirective);


})(window.angular);
