<div class="portlet light">
    <div class="portlet-body" id="chats">
        <div id="pfc_content_expandable">
            <div id="pfc_channels">
                <ul id="pfc_channels_list"></ul>
                <div id="pfc_channels_content"></div>
            </div>
            <!--<div id="pfc_input_container">
    
                <table style="margin:0;padding:0;border-collapse:collapse;">
                    <tbody>
                        <tr>
                        <td class="pfc_td1">
                            <p id="pfc_handle"></p>      
                        </td>
                        </tr>
                    </tbody>
                </table>
            </div>-->
        
            <!--<div id="pfc_colorlist"></div>-->
        </div> 
        <div class="chat-form">
            <div class="input-cont">
                <input class="form-control" type="text" id="pfc_words" placeholder="Type a message here..."/>
            </div>
            <div class="btn-cont">
                <span class="arrow"> </span>
                <a href="javascript:pfc.doSendMessage();" id="pfc_send" class="btn blue icn-only">
                    <i class="fa fa-check icon-white"></i>
                </a>
            </div>
        </div>
    </div>
    <div id="pfc_errors" style="display:none"></div>
    <div id="pfc_smileys" style="display:none"></div>
</div>
