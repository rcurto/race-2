/***
 Metronic AngularJS App Main Script
 ***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    'core',
    'ui.bootstrap.datetimepicker',
    'angular-table',
    'gettext',
    'parseDirective',
    'naif.base64',
    'pubnub.angular.service',
    'ui.utils.masks',
    'ui.select2'
]);

/* Setup global settings  */
MetronicApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', 'userFactory', function ($scope, $rootScope, userFactory) {
    var userPromise = userFactory.auth().then(function (user) {
        if (user == null) {
            window.location.href = 'index.html';
        }
        $scope.user = user;
    });

    $scope.$on('$viewContentLoaded', function () {
        Metronic.initComponents(); // init core components
    });

    Parse.Promise.when([userPromise]).then(function () {
        $scope.loaded = true;
        if ($scope.initPage)
            $scope.initPage()
    });

    $scope.$watch('initPage', function (value) {
        if ($scope.loaded == true)
            $scope.initPage();
    }, true)
}]);


/* Setup Layout Part - Quick Sidebar */
MetronicApp.controller('QuickSidebarController', ['$scope', function ($scope) {
    /* //Change by kazuyou@
     setTimeout(function () {
     QuickSidebar.init(); // init quick sidebar
     }, 2000)
     */
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url

    var currentUser = Parse.User.current();
    if (currentUser && currentUser.id == 'kWtTVG3FGB') {
        $urlRouterProvider.otherwise("/races");
    } else {
        $urlRouterProvider.otherwise("/dashboard");
    }

    $stateProvider.state('index', {
        url: '/',
        templateUrl: 'apps/mainApp/race/raceList.html',
        controller: 'raceListController',
        if_guest: 'login'
    }).state('dashboard', {
        url: '/dashboard',
        templateUrl: 'apps/mainApp/dashboard/dashboard.html',
        controller: 'dashboardController',
        if_guest: 'login'
    }).state('tops', {
        url: '/tops',
        templateUrl: 'apps/mainApp/tops/tops.html',
        controller: 'topsController',
        if_guest: 'login'
    }).state('chat', {
        url: '/chat',
        templateUrl: 'apps/mainApp/chat/chat.html',
        controller: 'chatController',
        if_guest: 'login'
    }).state('my-goals', {
        url: '/my-goals',
        templateUrl: 'apps/mainApp/myGoals/myGoals.html',
        controller: 'myGoalsController',
        if_guest: 'login'
    }).state('race', {
        url: "/races",
        templateUrl: 'apps/mainApp/race/raceList.html',
        controller: 'raceListController',
        data: {pageTitle: 'Races'},
        if_guest: 'login'
    }).state('race-athletes', {
        url: '/races/athletes',
        templateUrl: 'apps/mainApp/race/athleteRaceList.html',
        controller: 'myRaceListController',
        data: {pageTitle: 'Races of All Athletes'},
        if_guest: 'login'
    }).state('race-add', {
        url: '/races/add',
        templateUrl: 'apps/mainApp/race/raceForm.html',
        controller: 'raceFormController',
        data: {pageTitle: 'Races'},
        if_guest: 'login'
    }).state('race-edit', {
        url: '/races/edit/:id',
        templateUrl: 'apps/mainApp/race/raceForm.html',
        controller: 'raceFormController',
        data: {pageTitle: 'Races'},

        if_guest: 'login'
    }).state('race-my', {
        url: '/races/my',
        templateUrl: 'apps/mainApp/race/myRaceList.html',
        controller: 'myRaceListController',
        data: {pageTitle: 'My Races'},
        if_guest: 'login'
    }).state('profile', {
        url: '/profile',
        templateUrl: 'apps/mainApp/profile/profile.html',
        controller: 'profileController',
        data: {pageTitle: 'Profile'},
        if_guest: 'login'
    }).state('feedbacks', {
        url: '/feedbacks',
        templateUrl: 'apps/mainApp/feedback/feedbackList.html',
        controller: 'feedbackController',
        data: {pageTitle: 'Feedbacks'},
        if_guest: 'login'
    }).state('requests', {
        url: '/requests',
        templateUrl: 'apps/mainApp/request/requestList.html',
        controller: 'requestController',
        data: {pageTitle: 'Requests'},
        if_guest: 'login'
    }).state('trainings', {
        url: '/trainings',
        templateUrl: 'apps/mainApp/training/trainingList.html',
        controller: 'trainingController',
        data: {pageTitle: 'Trainings'},
        if_guest: 'login'
    }).state('tips', {
        url: '/tips',
        templateUrl: 'apps/mainApp/tip/tipList.html',
        controller: 'tipListController',
        data: {pageTitle: 'Tips'},
        if_guest: 'login'
    }).state('faq', { //Add by kazuyou@
        url: '/faq',
        templateUrl: 'apps/mainApp/faq/faq.html',
        controller: 'faqListController',
        data: {pageTitle: 'FAQ'},
        if_guest: 'login'
    })


}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", "gettextCatalog", "userFactory", function ($rootScope, settings, $state, gettextCatalog, userFactory) {

    moment.locale('en',{
        week : {
            dow : 1, // Monday is the first day of the week.
        }
    });


    $rootScope.$state = $state; // state to be accessed from view
    gettextCatalog.debug = true;
    var currencyUser = null;
    $rootScope.$watch(function () {
        return $state.current
    }, function (value) {
        if (value == undefined) {
            return;
        }
        userFactory.auth(function (result) {
            currencyUser = result;
            if (currencyUser != null) {
                gettextCatalog.setCurrentLanguage(currencyUser.language);
            } else {
                gettextCatalog.setCurrentLanguage('english');
            }
            if (value['if_guest'] != undefined && currencyUser == null) {
                $state.go(value['if_guest']);
            }
            if (value['if_user'] != undefined && currencyUser != null) {
                $state.go(value['if_user']);
            }

            /*if (value['access'] != undefined && currencyUser != null && !currencyUser.hasAccess(value['access'])) {
             $state.go('index')
             }*/
        })
    }, true);
}]).filter('orderObjectBy', function(){
    return function(input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            a = (a['attributes'][attribute]);
            b = (b['attributes'][attribute]);
            if (a > b) {
                return 1;
            }
            if (a < b) {
                return -1;
            }
            return 0;
        });
        return array;
    }
})